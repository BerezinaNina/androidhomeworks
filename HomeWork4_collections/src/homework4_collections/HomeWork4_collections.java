
package homework4_collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class HomeWork4_collections {

  
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
     
        ArrayList<Float> arrNumbers = new ArrayList<Float>();       
        Float fVal = new Float(0);
        do{
            System.out.println("Введите не отрицательное значение:");
            fVal = scan.nextFloat();
            if (arrNumbers.contains(fVal)){
                arrNumbers.remove(fVal);
            } else {
                        arrNumbers.add(fVal);
                   }
        } while (fVal>0); 
        arrNumbers.remove(fVal);
        
        System.out.println("Введенный массив:  "+arrNumbers);
        
        ArrayList<Float> arrNumbersSorted = new ArrayList<Float>();
        arrNumbersSorted.addAll(arrNumbers);
        Collections.sort(arrNumbersSorted);
        System.out.println("Отсортированный массив: " + arrNumbersSorted);
        
        float fMean = 0;
        int n=arrNumbers.size();
        
        for (int i=0; i<n; i++){
            fMean+=arrNumbers.get(i);
        }
        
        fMean=fMean/n;
        System.out.println("Среднее арифметическое: "+fMean);
        System.out.print("Числа, которые не меньше среднего арифметического в порядке ввода: ");
        for (int i=0; i<n; i++){
            if ((arrNumbers.get(i)>fMean)||(arrNumbers.get(i)==fMean)){
                System.out.print(arrNumbers.get(i)+"  ");
            }
        }
        System.out.println();
    }
}

