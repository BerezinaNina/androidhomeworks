package homework2_logical;

import java.util.Scanner;

// В общем я решила, что мне пойдет на пользу решить все варианты))))

public class HomeWork2_Logical 
{
    public static int firstVar(int nNum1, int nNum2)
    {
        int nRes=0;
        if ((nNum1>nNum2)&&(nNum1%2==0))
            {
                nRes=nNum1-nNum2;
            }
        else if ((nNum2>nNum1)&&(nNum2%2==0))
            {
                nRes=nNum2-nNum1;
            }
        else if (((nNum1>nNum2)&&(nNum1%2!=0))||((nNum2>nNum1)&&(nNum2%2!=0)))
            {
                nRes=nNum1+nNum2;
            }
        else if (nNum1==nNum2)
            {
                nRes=nNum1*nNum2;
            }
        return nRes;
    }
    
    public static int secondVar(int nNum1, int nNum2)
    {
        int nRes=0;
        if (nNum1%nNum2==0)
            {
                nRes=nNum1*nNum2;
            }
        else
            {
                nRes=nNum2*nNum2;
            }
        return nRes;
    }
    
    public static int thirdVar(int nNum1, int nNum2)
    {
        int nRes=0;
        if (((nNum1%2==0)&&(nNum2%2==0))||((nNum1%2!=0)&&(nNum2%2!=0)))
            {
                nRes=nNum1*nNum2;
            }
        else 
            {
                nRes=nNum1+nNum2;
            }
        return nRes;
    }
    
    public static int fourthVar(int nNum1, int nNum2)
    {
        int nRes=0;
        if (((nNum1>nNum2)&&(nNum1%2==0))||((nNum2>nNum1)&&(nNum2%2==0)))
            {
                nRes=nNum1+nNum2;
            }
        else if (((nNum1>nNum2)&&(nNum1%2!=0))||((nNum2>nNum1)&&(nNum2%2!=0)))
            {
                nRes=nNum1*nNum2;
            }
        else if (nNum1==nNum2)
            {
                nRes=-1;
            }
        return nRes;
    }
    
    public static int fifthVar(int nNum1, int nNum2)
    {
        int nRes=0;
        if (((nNum1>nNum2)&&(nNum1%5==0))||((nNum2>nNum1)&&(nNum2%5==0)))
            {
                nRes=nNum1*nNum2;
            }
        else 
            {
                nRes=nNum1+nNum2;
            }
           
        return nRes;
    }
    
    public static int sixthVar(int nNum1, int nNum2)
    {
        int nRes=0;
          if (((nNum1>0)&&(nNum2>0))||((nNum1<0)&&(nNum2<0)))
            {
                nRes=nNum1*nNum2;
            }
        else 
            {
                nRes=nNum1+nNum2;
            }
        return nRes;
    }
      
    public static void main(String[] args) 
    {
        Scanner input = new Scanner (System.in);
        
        System.out.println("Будьте добры, введите первое число");
        int nNumber1 = input.nextInt();
        
        System.out.println("Будьте добры, введите второе число");
        int nNumber2 = input.nextInt();
        
        System.out.println("Будьте добры, введите номер варианта");
        int nVar = input.nextInt();
        
        int nRes=0;
        
        if (nVar==1)
                {
                    nRes=firstVar(nNumber1, nNumber2);
                }
        else if (nVar==2)
                {
                    nRes=secondVar(nNumber1, nNumber2);
                }
        else if (nVar==3)
                {
                    nRes=thirdVar(nNumber1, nNumber2);
                }
        else if (nVar==4)
                {
                    nRes=fourthVar(nNumber1, nNumber2);
                }
        else if (nVar==5)
                {
                    nRes=fifthVar(nNumber1, nNumber2);
                }
        else if (nVar==6)
                {
                    nRes=sixthVar(nNumber1, nNumber2);
                }
        System.out.println("Результат вычислений равен  " + nRes);
    }
    
}
