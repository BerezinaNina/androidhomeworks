
package homework3_string;

import java.util.Scanner;

public class Homework3_string {

    public static void main(String[] args) {
        
        String [] arrSign = {".", ",", "!", "?", "-", ":", ";"};
        System.out.println("Введите строку");
        
        Scanner scan = new Scanner (System.in);
        String strLine=scan.nextLine();
        
        for (int i=0; i<arrSign.length; i++){
            strLine = strLine.replace(arrSign[i]," ");
        }
        
        int nCount;
        do{
            nCount=strLine.length();
            strLine = strLine.replace("  "," ");  
        }while (strLine.length()!= nCount);
        
       
        System.out.println("Строка, в которой все разделители заменены на пробелы. Лишние пробелы удалены.");
        System.out.println(strLine);
        System.out.println("Слова, с которых состоит строка:");
                
        String [] arrWords =strLine.split(" ");
        
        for (String arrWord : arrWords) {
            System.out.println(arrWord);
        }
    }
    
}
