// Я потратила ооооочень много времени (часа три точно), пытаясь выполнение второго варианта запихнуть в одну функцию. Это вообще возможно? 

package homework2_recursion;

import java.util.Scanner;

public class HomeWork2_recursion
{

    public static int numbersBetween(int nMin, int nMax)
    {
        if (nMin<(nMax-1))
        {
            System.out.print(nMin+1+"  ");
            nMin=numbersBetween(nMin+1,nMax);
        }
        else if (nMin==nMax)
            {
                System.out.println("Вы ввели два одинаковых числа. Не надо так((((");
            }
        return nMin;
    }
   
    public static int pow(int nNumb, int nPow)
    {
        int nResult=0;
       
        if (nPow<=0)
            {
                nResult = -1;
            }
        else
            {
                nResult = nNumb*pow(nNumb, nPow-1);
            }
        return nResult;
    }
    
    public static int factorial(int nNumb)
    {
        int nResult=0;
        if (nNumb<=1)
            {
                return 1;
            }
        else 
            {
                nResult = factorial(nNumb-1)*nNumb;
            }
        return nResult;
    }
    
    public static void main(String[] args) 
    {
        Scanner input=new Scanner (System.in); 
        
        System.out.println("Пожалуйста, введите номер варианта (1 или 2)");
        int nVar = input.nextInt();
        
        if (nVar==1)
        {
            System.out.println("Пожалуйста, введите меньшее целое число");
            int nMin = input.nextInt();
        
            System.out.println("Пожалуйста, введите большее целое число");
            int nMax = input.nextInt();
        
            System.out.println("Целые числа между минимальным и максимальным числом:");
            nMin = numbersBetween(nMin, nMax);
        }
        else if (nVar==2)
            {
                System.out.println("Пожалуйста, введите целое число");
                int nNumber = input.nextInt();
                if (nNumber<0)
                {
                    nNumber=pow(nNumber, 10); 
                }
                else if (nNumber>0)
                {
                    nNumber=factorial(nNumber);
                }
                System.out.println(nNumber);
            }
        else 
            {
                System.out.println("Вы должны были выбрать вариант 1 или 2");
            }
        
    }
    
}   