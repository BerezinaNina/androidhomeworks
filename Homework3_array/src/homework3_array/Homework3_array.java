
package homework3_array;

import java.util.Scanner;


public class Homework3_array {

    public static void main(String[] args) {
        Scanner scan = new Scanner (System.in); 
                
        System.out.println("Пожалуйста, введите ЧЕТНОЕ колличество элементов массива: ");
        int n = scan.nextInt();
        if (n%2!=0){
            System.out.println("ЧЕТНОЕ - значит без остатка делится на два. Пожалуйста, повторите ввод: ");
            n = scan.nextInt();
        }
        
        float[] arrNumb = new float [n];
        System.out.println("Пожалуйста, введите числа: ");
        
        float nMultiplicationOfFirstHalfElements=1;
        float nMultiplicationOfSecondHalfElements=1;
        float nSumOfElementsWithEvenIndex=0;
        float nSumOfElementsWithUnevenIndex=0;
        
        
        for (int i=0; i<n; i++){
            arrNumb[i]=scan.nextInt();
        }
        float nMax=arrNumb[0];
        float nMin=arrNumb[0];
        for (int i=0; i<n; i++){
             if (arrNumb[i]>nMax){
                nMax=arrNumb[i];
            }
            else if (arrNumb[i]<nMin){
                nMin=arrNumb[i];
            }
            if (i<(n/2)){
                nMultiplicationOfFirstHalfElements*=arrNumb[i];
            }
            else if (i<n){
                nMultiplicationOfSecondHalfElements*=arrNumb[i];
            }
            if ((i==0)||(i%2==0)){
                nSumOfElementsWithEvenIndex+=arrNumb[i];
            }
            else if (i%2!=0){
                nSumOfElementsWithUnevenIndex+=arrNumb[i];
            }
           
        }
        
        System.out.println("Максимальное число массива:  " + nMax);
        System.out.println("Минимальное число массива:  " + nMin);
        System.out.println("Произведение всех чисел с первой половины массива:  " + nMultiplicationOfFirstHalfElements);
        System.out.println("Произведение всех чисел со второй половины массива:  " + nMultiplicationOfSecondHalfElements);
        System.out.println("Сумма всех элементов с четными индексами (и с индексом 0):  " + nSumOfElementsWithEvenIndex);
        System.out.println("Сумма всех элементов с нечетными индексами:  " + nSumOfElementsWithUnevenIndex);
        
    }
    
}
